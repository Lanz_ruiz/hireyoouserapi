const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Metrics = new Schema(
    {
        chest: { type: Number, required: true },
        waist: { type: Number, required: true },
        hips: { type: Number, required: true },
        gender: { type: String, required: true },
        userId: { type: String, required: true },
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('metrics', Metrics)