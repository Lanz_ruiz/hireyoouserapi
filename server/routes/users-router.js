const express = require('express')
require('dotenv').config()
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');


const UserCtrl = require('../controllers/users-ctrl')
const AuthCtrl = require('../controllers/auth-ctrl')
const jwt = require('jsonwebtoken')


const router = express.Router()

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(403)
      req.user = user
      next()
    })
}



const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "HireYoo API",
      version: '1.0.0',
    },
  },
  apis: ["index.js"],
};



// router.post('/create', UserCtrl.createUsers)
router.get('/:id', UserCtrl.getUserById)
router.post('/signup', UserCtrl.signUp)
router.post('/login', AuthCtrl.login)
router.post('/mobile/google/signin', AuthCtrl.mobileSignGoogle)
router.post('/mobile/facebook/signin', AuthCtrl.mobileSignFacebook)
router.post('/token', AuthCtrl.token)
router.delete('/', authenticateToken, UserCtrl.deleteUser)
router.put('/', authenticateToken, UserCtrl.updateUser)



module.exports = router